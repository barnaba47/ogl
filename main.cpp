#include "mesh.h"
#include "objects.h"

// Window dimensions
GLint width = 960;
GLint height = 540;

// Camera 
GLfloat speed = 0.08f;
GLfloat speedup = 0.16f;
GLfloat defspeed = speed;
GLfloat sensitivity = 100.0f;
GLfloat fov = 75.0f;


int main()
{
    // Initialize GLFW
    glfwInit();

    // Set OpenGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	
    // Tell GLFW we are using the CORE profile
	// So that means we only have the modern functions
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create a GLFW window
    GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL", nullptr, nullptr);
    
    if(!window)
    {
        fprintf(stderr, "Error: Could not open the window!\n");
        glfwTerminate();
        return -1;
    }
    
    // Introduce the window into the current context
    glfwMakeContextCurrent(window);
    
    // Init GLEW (loads extensions?)
    glewExperimental = GL_TRUE;
    glewInit();

    // Print device & OpenGL info
    const GLubyte* renderer = glGetString(GL_RENDERER);
    const GLubyte* version = glGetString(GL_VERSION);
    printf("Renderer: %s\n", renderer);
    printf("OpenGL version supported %s\n", version);

    // Setting viewport 
	glViewport(0, 0, width, height);
    
    // Other OGL settings
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE); 
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(1.5);
    
    // Camera
    Camera camera(width, height, glm::vec3(0.0f, 0.6f, 2.0f), speed, speedup, defspeed, sensitivity);


    // Textures
    Texture textures_dirt[]
    {
        Texture("res/textures/dirt.png", "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE),
        Texture("res/textures/dirt.png", "specular", 1, GL_RED, GL_UNSIGNED_BYTE)
    };

    Texture textures_plank[]
    {
        Texture("res/textures/planks.png", "difusse", 0, GL_RGBA, GL_UNSIGNED_BYTE),
        Texture("res/textures/planksSpec.png", "specular", 1, GL_RED, GL_UNSIGNED_BYTE)
    };
    

	// Generates Shader object using shaders default.vert and default.frag
	Shader planeShader("shaders/plane.vert", "shaders/plane.frag");

	// Store mesh data in vectors for the mesh
	std::vector <Vertex> verts(planeVertices, planeVertices + sizeof(planeVertices) / sizeof(Vertex));
	std::vector <GLuint> ind(planeIndices, planeIndices + sizeof(planeIndices) / sizeof(GLuint));
	std::vector <Texture> tex(textures_plank, textures_plank + sizeof(textures_plank) / sizeof(Texture));
	// Create floor mesh
	Mesh floor(verts, ind, tex);

    glm::vec3 planePos = glm::vec3(0.0f, -1.0f, 0.0f);
    glm::vec3 planeScale = glm::vec3(8.0f, 0.0f, 7.0f);
	glm::mat4 planeModel = glm::mat4(1.0f);

	planeModel = glm::translate(planeModel, planePos);
    planeModel = glm::scale(planeModel, planeScale);
    
    // Rotation (main loop)
    //objectModel = glm::rotate(trans2, glm::radians(-90.0f), glm::vec3(1.0, 0.0, 0.0));

    // unused uniforms
    //GLuint uniID = glGetUniformLocation(shaderProgram.ID, "u_time");
    //GLuint uniID2 = glGetUniformLocation(shaderProgram.ID, "u_resolution");
    //GLuint uniID3 = glGetUniformLocation(shaderProgram.ID, "u_mouse_pos");


    // Light shader
    Shader lightShader("shaders/light.vert", "shaders/light.frag");
    // Store mesh data in vectors for the mesh
	std::vector <Vertex> lightVerts(lightVertices, lightVertices + sizeof(lightVertices) / sizeof(Vertex));
	std::vector <GLuint> lightInd(lightIndices, lightIndices + sizeof(lightIndices) / sizeof(GLuint));
    // Crate light mesh
    Mesh light(lightVerts, lightInd, tex);

    glm::vec3 lightPos = glm::vec3(0.0f, 8.0f, 0.0f);

    glm::mat4 lightModel = glm::mat4(1.0f);

    glm::vec4 lightColor = glm::vec4(1.1f, 1.1f, 1.0f, 1.0f);
    lightModel = glm::translate(lightModel, lightPos);

    // Cube shader
    Shader cubeShader("shaders/cube.vert", "shaders/cube.frag");
    // Store mesh data in vectors for the mesh
	std::vector <Vertex> cubeVerts(cubeVertices, cubeVertices + sizeof(cubeVertices) / sizeof(Vertex));
	std::vector <GLuint> cubeInd(cubeIndices, cubeIndices + sizeof(cubeIndices) / sizeof(GLuint));
    std::vector <Texture> texd(textures_dirt, textures_dirt + sizeof(textures_dirt) / sizeof(Texture));
    // Crate cube mesh
    Mesh cube(cubeVerts, cubeInd, texd);

    glm::vec3 cubePos = glm::vec3(0.0f, 0.6f, -0.5f);
    glm::vec3 cubeScale = glm::vec3(0.8f, 0.8f, 0.8f);
    glm::mat4 cubeModel = glm::mat4(1.0f);

    //glm::vec4 cubeColor = glm::vec4(1.1f, 1.1f, 1.0f, 1.0f);
    cubeModel = glm::scale(cubeModel, cubeScale);
    cubeModel = glm::translate(cubeModel, cubePos);


    // Shaders activation 
    lightShader.Activate();
    glUniformMatrix4fv(glGetUniformLocation(lightShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(lightModel));
    glUniform4f(glGetUniformLocation(lightShader.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);
    
    planeShader.Activate();
    glUniformMatrix4fv(glGetUniformLocation(planeShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(planeModel));
    glUniform4f(glGetUniformLocation(planeShader.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);
    glUniform3f(glGetUniformLocation(planeShader.ID, "lightPos"), lightPos.x, lightPos.y, lightPos.z);

    cubeShader.Activate();
    glUniformMatrix4fv(glGetUniformLocation(cubeShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(cubeModel));
    glUniform4f(glGetUniformLocation(cubeShader.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);
    glUniform3f(glGetUniformLocation(cubeShader.ID, "lightPos"), lightPos.x, lightPos.y, lightPos.z);



    // MAIN LOOP

    while(!glfwWindowShouldClose(window))
    {     
        // Handles camera inputs
		camera.Inputs(window);
        camera.updateMatrix(fov, 0.01f, 100.0f);

        // Set background color    
        glClearColor(0.136f, 0.136f, 0.136f, 1.0f);
        // Wipe the drawing surface clear
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // Cull face setting
        //glCullFace(GL_BACK);
        glCullFace(GL_FRONT);

    
        
        // Draws different meshes
		floor.Draw(planeShader, camera);

        cube.Draw(cubeShader, camera);
        cubeModel = glm::rotate(cubeModel, glm::radians(0.1f), glm::vec3(1.0, 1.0, 0.0));
        glUniformMatrix4fv(glGetUniformLocation(cubeShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(cubeModel));
        //cubeModel = glm::rotate(cubeModel, (float)glfwGetTime()/1024, glm::vec3(0.1, 0.1, 0.0));

		light.Draw(lightShader, camera);
        lightModel = glm::translate(lightModel, glm::vec3(0.1, 0.0, 0.0));

        // Swap the back buffer with the front buffer
		glfwSwapBuffers(window);
		// Take care of all GLFW events
		glfwPollEvents();
    }

    // Delete all created
	planeShader.Delete();
    cubeShader.Delete();
	lightShader.Delete();

	// Delete window before ending the program
	glfwDestroyWindow(window);
	// Terminate GLFW before ending the program
	glfwTerminate();
	return 0;
}
