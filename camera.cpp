#include "camera.h"

Camera::Camera(int width, int height, glm::vec3 position, float speed, float speedup, float defspeed, float sensitivity)
{
    Camera::width = width;
    Camera::height = height;
    Position = position;
    Camera::speed = speed;
    Camera::speedup = speedup;
    Camera::defspeed = defspeed;
    Camera::sensitivity = sensitivity;

}

void Camera::updateMatrix(float FOVdeg, float nearPlane, float farPlane)
{
    glm::mat4 view = glm::mat4(1.0f);
    glm::mat4 projection = glm::mat4(1.0f);

    view = glm::lookAt(Position, Position + Orientation, Up);
    projection = glm::perspective(glm::radians(FOVdeg), (float)width / height, nearPlane, farPlane);

    cameraMatrix = projection * view;
    
}

void Camera::Matrix(Shader& shader, const char* uniform1, const char* uniform2, const char* uniform3)
{
    glUniformMatrix4fv(glGetUniformLocation(shader.ID, uniform1), 1, GL_FALSE, value_ptr(cameraMatrix));
    glUniformMatrix4fv(glGetUniformLocation(shader.ID, uniform2), 1, GL_FALSE, value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(shader.ID, uniform2), 1, GL_FALSE, value_ptr(projection));

}

void Camera::Inputs(GLFWwindow* window)
{
    // Handles key inputs

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		Position += speed * Orientation;
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		Position += speed * -glm::normalize(glm::cross(Orientation, Up));
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		Position += speed * -Orientation;
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		Position += speed * glm::normalize(glm::cross(Orientation, Up));
	}

	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		Position += speed * Up;
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		Position += speed * -Up;
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		speed = speedup;
	}

    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	{
		speed = defspeed;
	}

    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS && firstClick == false)
    {
        std::this_thread::sleep_for(100ms);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        glfwSetCursorPos(window, (width / 2), (height / 2));
        firstClick = true;
    }

    else if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS && firstClick == true)
	{
        std::this_thread::sleep_for(100ms);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        firstClick = false;
    }


    // Handles mouse inputs

    // Hides mouse cursor
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    
    if (firstClick)
    {
        // Stores the coordinates of the cursor
        double mouseX;
        double mouseY;

        // Fetches the coordinates of the cursor
        glfwGetCursorPos(window, &mouseX, &mouseY);

        // Normalizes and shifts the coordinates of the cursor such that they begin in the middle of the screen
        // and then "transforms" them into degrees 
        float rotX = sensitivity * (float)(mouseY - (height / 2)) / height;
        float rotY = sensitivity * (float)(mouseX - (width / 2)) / width;

        // Calculates upcoming vertical change in the Orientation
        glm::vec3 newOrientation = glm::rotate(Orientation, glm::radians(-rotX), glm::normalize(glm::cross(Orientation, Up)));

        // Decides whether or not the next vertical Orientation is legal or not
        if (abs(glm::angle(newOrientation, Up) - glm::radians(90.0f)) <= glm::radians(85.0f))
        {
            Orientation = newOrientation;
        }

        // Rotates the Orientation left and right
        Orientation = glm::rotate(Orientation, glm::radians(-rotY), Up);

        // Sets mouse cursor to the middle of the screen so that it doesn't end up roaming around
        glfwSetCursorPos(window, (width / 2), (height / 2));
    }

}