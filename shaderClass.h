#ifndef SHADER_CLASS_H
#define SHADER_CLASS_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cerrno>

std::string get_file_content(const char* filename);

class Shader 
{
    public:
        GLuint ID;
        Shader(const char* vertexFile, const char* fragmentFile, const char* geometryFile = "no_file");

        void Activate();
        void Delete();
        
    private:
	// Checks if the different Shaders have compiled properly
	void compileErrors(unsigned int shader, const char* type);    
};

#endif