#ifndef OBJECTS_CLASS_H
#define OBJECTS_CLASS_H

#include <GL/glew.h>

GLfloat triangleVertices[] = 
{  
//      Position              Color    
    -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,  // bottom left
    0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,  // bottom right
    0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f   // top 
};


GLuint triangleIndices[] =
{
	0, 1, 2, // triangle
};


GLfloat pointVerices[] = 
{  
//      Position           Color          Size
    0.0f, 0.0f, 0.0f,  1.0f, 1.0f, 1.0f,  1.0f
};

Vertex cubeVertices[] =
{
                    //      Positions                      Normals                      Colors                         Texture coords         
	Vertex{glm::vec3(-1.0f, 1.0f,  1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f)},	// 0	// top
	Vertex{glm::vec3(-1.0f, 1.0f, -1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 1.0f)},	// 1		
	Vertex{glm::vec3(1.0f, 1.0f, -1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 1.0f)},	// 2		
	Vertex{glm::vec3(1.0f, 1.0f,  1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f)}, // 3	

    Vertex{glm::vec3(-1.0f, -1.0f,  1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f)},	// 4    // bottom
	Vertex{glm::vec3(-1.0f, -1.0f, -1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 1.0f)},	// 5		
	Vertex{glm::vec3(1.0f, -1.0f, -1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 1.0f)},	// 6		
	Vertex{glm::vec3(1.0f, -1.0f,  1.0f),	glm::vec3(0.0f, -1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f)},	// 7

    Vertex{glm::vec3(-1.0f, -1.0f,  1.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f)},	// 4 8   // front
    Vertex{glm::vec3(-1.0f, 1.0f,  1.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 1.0f)},	// 0 9 
    Vertex{glm::vec3(1.0f, 1.0f,  1.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 1.0f)}, // 3 10
    Vertex{glm::vec3(1.0f, -1.0f,  1.0f),	glm::vec3(0.0f, 0.0f, 1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f)},	// 7 11

	Vertex{glm::vec3(-1.0f, 1.0f, -1.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f)},	// 1 12  // back
	Vertex{glm::vec3(-1.0f, -1.0f, -1.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 1.0f)},	// 5 13
	Vertex{glm::vec3(1.0f, -1.0f, -1.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 1.0f)},	// 6 14
	Vertex{glm::vec3(1.0f, 1.0f, -1.0f),	glm::vec3(0.0f, 0.0f, -1.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f)},	// 2 15
    
    Vertex{glm::vec3(-1.0f, -1.0f, -1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f)},	// 5 16  // left
    Vertex{glm::vec3(-1.0f, 1.0f, -1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 1.0f)},	// 1 17
    Vertex{glm::vec3(-1.0f, 1.0f,  1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 1.0f)},	// 0 18
    Vertex{glm::vec3(-1.0f, -1.0f,  1.0f),	glm::vec3(-1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f)},	// 4 19

    Vertex{glm::vec3(1.0f, -1.0f,  1.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f)},	// 7 20   //right
    Vertex{glm::vec3(1.0f, 1.0f,  1.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 1.0f)}, // 3 21
    Vertex{glm::vec3(1.0f, 1.0f, -1.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 1.0f)},	// 2 22
    Vertex{glm::vec3(1.0f, -1.0f, -1.0f),	glm::vec3(1.0f, 0.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f)}	// 6 23
	
};

GLuint cubeIndices[] =
{
    0, 1, 2,
    0, 2, 3,
    5, 4, 7,
    5, 7, 6,
    8, 9, 10,
    8, 10, 11,
    12, 13, 14,
    12, 14, 15,
    16, 17, 18,
    16, 18, 19,
    20, 21, 22,
    20, 22, 23
};

Vertex lightVertices[] =
{
    //                    Positions
    Vertex{glm::vec3(-0.2f, 0.2f, 0.2f)},   // front face
    Vertex{glm::vec3(0.2f, 0.2f, 0.2f)},    
    Vertex{glm::vec3(0.2f, -0.2f, 0.2f)},  
    Vertex{glm::vec3(-0.2f, -0.2f, 0.2f)},  

    Vertex{glm::vec3(-0.2f, 0.2f, -0.2f)},  // back face
    Vertex{glm::vec3(0.2f, 0.2f, -0.2f)},  
    Vertex{glm::vec3(0.2f, -0.2f, -0.2f)},
    Vertex{glm::vec3(-0.2f, -0.2f, -0.2)} 
};

GLuint lightIndices[] =
{
	0, 2, 3,
    0, 1, 2,
    0, 4, 1,
    4, 5, 1,
    3, 2, 7,
    2, 6, 7,
    5, 7, 6,
    5, 4, 7,
    4, 0, 3,
    3, 7, 4,
    1, 5, 6,
    1, 6, 2  
};

Vertex planeVertices[] =
{//                       Positions                 Normals                         Colors                          Texture coords     
	Vertex{glm::vec3(-1.0f, 0.0f,  1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f)},		
	Vertex{glm::vec3(-1.0f, 0.0f, -1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(0.0f, 1.0f)},		
	Vertex{glm::vec3(1.0f, 0.0f, -1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 1.0f)},		
	Vertex{glm::vec3(1.0f, 0.0f,  1.0f),	glm::vec3(0.0f, 1.0f, 0.0f),	glm::vec3(0.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f)}		
};

GLuint planeIndices[] =
{
    0, 1, 2,
    0, 2, 3
};

#endif