#define GLM_ENABLE_EXPERIMENTAL
#ifndef CAMERA_CLASS_H
#define CAMERA_CLASS_H

#include <iostream>
#include <cstdlib>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "shaderClass.h"

#include <chrono>
#include <thread>

using namespace std::chrono_literals;


class Camera
{
    public:
        bool firstClick = false;
        glm::vec3 Position;
        glm::vec3 Orientation = glm::vec3(0.0f, 0.0f, -1.0f);
        glm::vec3 Up = glm::vec3(0.0f, 1.0f, 0.0f);
        glm::mat4 cameraMatrix = glm::mat4(1.0f);
        glm::mat4 view = glm::mat4(1.0f);
        glm::mat4 projection = glm::mat4(1.0f);

        int width;
        int height;

        float speed;
        float speedup;
        float defspeed;
        float sensitivity;

        Camera(int width, int height, glm::vec3 Position, float speed, float speedup, float defspeed, float sensitivity);

        void updateMatrix(float FOVdeg, float nearPlane, float farPlane);
        void Matrix(Shader& shader, const char* cameraMatrix, const char* view, const char* projection);
        
        void Inputs(GLFWwindow* window);

};

#endif
