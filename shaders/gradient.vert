#version 460

layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aColor;
out vec3 color;


// Changing values
uniform mat4 camMatrix;
uniform float u_time;

void main() 
{
  gl_Position = camMatrix * vec4(aPos, 1.0f);
  color = vec3(aColor);
}