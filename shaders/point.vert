#version 460

layout (location=0) in vec3 Pos;
layout (location=1) in vec3 Color;
layout (location=2) in float Size;

out vec3 vColor;
out float vSize;

void main() 
{
    gl_Position = vec4(Pos, 1.0f);
    vSize = Size;
    vColor = Color;   
}