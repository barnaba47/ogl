#version 460

layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aColor;
out vec3 color;


// Changing values
uniform mat4 camMatrix;
uniform float u_time;
uniform vec2 u_size;
void main() 
{
    gl_Position = camMatrix * vec4(aPos, 1.0f);
    //color = aColor;
    //color = vec3(0.2, 0.4, 0.8);
    color = vec3(0.2, 0.4,0.10*sin(u_time*2.0)+0.75);
}