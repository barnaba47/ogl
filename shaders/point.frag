#version 460

out vec4 FragColor;

in vec3 gColor;

void main()
{
    FragColor = vec4(gColor, 1.0f);
}