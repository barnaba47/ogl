#version 460

layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aColor;
out vec3 color;


// Changing values
uniform mat4 camMatrix;
uniform float u_time;


void main() 
{
  gl_Position =  camMatrix * vec4(aPos, 1.0f);

  float t = sin(u_time/75);

  if (aPos.y >= 0.5f)
  {
    vec3 tcolor = vec3(0.102+t/8, 0.5098, 0.7451)*t-(abs(0.1*t));
    color = vec3(tcolor);
  }

    if (aPos.y <= -0.5f)
  {
  
    vec3 bcolor = vec3(0.5373+abs(t-1.34), 0.8235+abs(t/6), 0.9882)*t;
    color = vec3(bcolor);
  }
  
}