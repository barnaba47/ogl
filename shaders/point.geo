#version 460

in vec3 vColor[];
out vec3 gColor;

in float vSize[];

layout(points) in;
layout(points, max_vertices = 256) out;

void main()
{   
    // set point size & color from main.cpp
    gl_PointSize = vSize[0];
    gColor = vColor[0];
    
    for (float i = 2.0f; i >= -2.0f; (i = i - 0.25f))
    {
        for (float j = 2.0f; j >= -2.0f; (j = j - 0.25f))
        {
            gl_Position = vec4(i, j, 0.0f, 1.0f);
            EmitVertex();     
        }
    }

    /* changes example
    gColor = vec3(1.0f, 0.0f, 0.0f);
    gl_Position = vec4(-0.85, 0.0, 0.0f, 1.0f);
    EmitVertex();
    gColor = vColor[0];
    EmitVertex();
    */

    EndPrimitive();
}  