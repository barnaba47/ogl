#version 460

layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aColor;
out vec3 color;


// Changing values
uniform mat4 camMatrix;
uniform mat4 u_transform;

void main() 
{
    gl_Position = camMatrix * u_transform * vec4(aPos, 1.0f);
    //color = aColor;
    color = vec3(1.0, 1.0, 1.0);
}