#version 460

out vec4 FragColor;

in vec3 color;
uniform vec2 u_resolution;
uniform vec2 u_mouse_pos;


void main() {
/*
  vec2 stu = gl_FragCoord.xy/u_resolution.xy;

  vec3 color1 = vec3(0.858, 0.964, 1);
  vec3 color2 = vec3(0.301, 0.678, 1);

  float mixValue = distance(stu,vec2(0.5,-0.25));
  vec3 fc = mix(color1,color2,mixValue);
*/
  FragColor = vec4(color,1.0);

}