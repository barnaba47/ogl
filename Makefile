CFLAGS= -std=c++20 -O2
LDFLAGS = -lglfw -lGL -lGLEW

OpenGL: main.cpp
	g++ $(CFLAGS) -o OpenGL *.cpp $(LDFLAGS)

.PHONY: test clean

test: OpenGL

clean:
	rm -f OpenGL