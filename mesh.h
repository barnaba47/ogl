#define GLM_ENABLE_EXPERIMENTAL
#ifndef MESH_CLASS_H
#define MESH_CLASS_H

#include <string>
#include <vector>

#include "vao.h"
#include "vbo.h"
#include "ebo.h"
//#include "objects.h"
#include "camera.h"
#include "texture.h"

class Mesh
{
    public:
    std::vector <Vertex> vertices;
    std::vector <GLuint> indices;
    std::vector <Texture> textures;

    // Store VAO in public so it can be used in the Draw function
    VAO VAo;
    
    // Initializes the mesh
    Mesh(std::vector <Vertex>& vertices, std::vector <GLuint>& indices, std::vector <Texture>& textures);
    
    // Draws the mesh
    void Draw(Shader& shader, Camera& camera);
};

#endif
